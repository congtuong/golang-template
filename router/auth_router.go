package router

import (
	controller "golang_template/controlller"

	"github.com/gin-gonic/gin"
)

func SetupAuthRouter(group *gin.RouterGroup) {
	auth_controller := controller.AuthController{}
	group.POST("/login", auth_controller.Login)
}
